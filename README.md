Autor: Ateldy Borges Brasil Filho 15/0006101

* As imagens deverão estar na pasta raíz do programa

Para compilar o programa:
1- Abra o terminal
2- Entre na pasta raiz do Programa
3- Use o comando 'make' e compile (caso esteja compilando novamente, use o comando 'make clean' antes de 'make')
4- Use o comando 'make run' e compile

5- Digite a opção desejada para o tipo de imagem
6- Digite o nome do arquivo
	- Caso tenha escolhido a opção PPM, crie um nome para casa saída (vermelha, verde, azul)
	- Serão criadas 3 imagens de saída, uma delas revelará a mensagem.
	- Caso tenha escolhido a opção PGM, a mensagem vai ser explícita no terminal.
