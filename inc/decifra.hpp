#ifndef DECIFRA_HPP
#define DECIFRA_HPP
#include <string>
#include <cstring>
#include <iostream>
#include <fstream>
#include <sstream>
#include <istream>
#include <fstream>
#include <cstdlib>
#include <cstdlib>
#include <iomanip>
#include <bitset>

using namespace std;

class Decifra {
  //Atributos
public:
    float altura; //std espaços de nomes
    float largura;
    string imagem;

public:
    //Contrutores
    Decifra();
    //Acessores
    void setTipoImagem (string imagem);
    string getTipoImagem();
    void setAltura (float altura);
    float getAltura();
    void setLargura(float largura);
    float getLargura();

};

#endif