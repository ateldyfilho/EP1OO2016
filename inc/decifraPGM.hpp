#ifndef DECIFRAPGM_HPP
#define DECIFRAPGM_HPP
#include "decifra.hpp"
#include "imagem.hpp"

using namespace std;

class DecifraPGM : public Decifra{ 

  public:
  	
    DecifraPGM();

    void Segredo_PGM();

};

#endif