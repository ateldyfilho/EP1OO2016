#ifndef DECIFRAPPM_HPP
#define DECIFRAPPM_HPP
#include "decifra.hpp"

using namespace std;

class DecifraPPM : public Decifra{

  public:
  	
    DecifraPPM();

    DecifraPPM(float largura, float altura);
    //void setLargura(float largura);
    //void setAltura(float altura);
    //void pixel_rgb(char R, char G, char B);
    //float getLargura();
    //float getAltura();
    void decifra_ppm();

};

#endif