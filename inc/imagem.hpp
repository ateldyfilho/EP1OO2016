#ifndef IMAGEM_HPP
#define IMAGEM_HPP
#include <string>

using namespace std;

class Imagem {
  //Atributos
private://protected
    string imagem;
    string mensagem;
    string numero_magico;
    float altura; 
    float largura;
    int numero_maximo_cor;

public:
    //Contrutores
    Imagem();
    ~Imagem();

    //Acessores
    void setTipoImagem (string imagem);
    void setAltura (float altura);
    void setLargura(float largura);
    void setMensagem(string mensagem);
    void setNumero_Magico(string numero_magico);
    void setNumero_Maximo_Cor(int numero_maximo_cor);
    void AbrirImagem();
    string getTipoImagem();

    float getAltura();
    float getLargura();
    string getMensagem();
    string getNumero_Magico();
    
    int getNumero_Magico_Cor();
    
};

#endif