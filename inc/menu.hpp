
#ifndef MENU_HPP
#define MENU_HPP
#include "imagem.hpp"
#include "decifraPGM.hpp"
#include "decifraPPM.hpp"

using namespace std;

class Menu {

  private:
    int opcao;

  public:
    Menu();
    void setOpcao(int opcao);
    int getOpcao();
    void MenuPrincipal();
};

#endif

