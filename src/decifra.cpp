#include <iostream>
#include "decifra.hpp"
#include <string>


using namespace std;

Decifra::Decifra(){
  setTipoImagem("Sem valor");
  setAltura(0);
  setLargura(0);

}

void Decifra::setTipoImagem(string imagem){
  this->imagem = imagem;
}
void Decifra::setAltura(float altura){
  this->altura = altura;
}
void Decifra::setLargura(float largura){
  this->largura = largura;
}
float Decifra::getAltura(){
  return altura;
}
float Decifra::getLargura(){
  return largura;
}
string Decifra::getTipoImagem(){
  return imagem;
}