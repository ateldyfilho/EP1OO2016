#include "imagem.hpp"
#include "decifraPPM.hpp"
#include <string>
#include <cstring>
#include <fstream>
#include <iostream>
#include <sstream>
#include <cstdlib>
#include <iomanip>
#include <fstream>
#include <bitset>
#include <fstream>

using namespace std;

DecifraPPM::DecifraPPM(){
  setLargura(0);
  setAltura(0);

}

void DecifraPPM::decifra_ppm(){
  FILE *arquivo_input;
  FILE *arquivo_output1;
  FILE *arquivo_output2;
  FILE *arquivo_output3;
  char nome_do_arquivo[100];
  char nome_arq_saida_verde[100];
  char nome_arq_saida_vermelho[100];
  char nome_arq_saida_azul[100];
  char R;
  char G;
  char B;
  char numero_magico[2];
  char mensagem[100];
  int altura;
  int largura;
  int numero_maximo_cor;
  int cont1, cont2;
  cout << "Nome da imagem PPM: "<< endl;
 
  scanf("%s", nome_do_arquivo);
  ifstream arquivo(nome_do_arquivo);
  arquivo_input = fopen(nome_do_arquivo, "rb");

  if(!arquivo_input){
     cout << "Erro ao abrir o arquivo: " << nome_do_arquivo << endl;
   }

  cout << "Nome da imagem PPM de saida (Vermelha): "<< endl;
  
  cin >> nome_arq_saida_vermelho;
  cout << "Nome da imagem PPM de saida (Verde): "<< endl;
  
  cin >> nome_arq_saida_verde;
  cout << "Nome da imagem PPM de saida (Azul): "<< endl;
 
  cin >> nome_arq_saida_azul;

  ifstream arquivo_saida1(nome_arq_saida_vermelho);
  arquivo_output1 = fopen(nome_arq_saida_vermelho, "wb");

  if(!arquivo_output1){
     cout << "Erro ao abrir o arquivo: " << nome_arq_saida_vermelho << endl;
   }

  ifstream arquivo_saida2(nome_arq_saida_verde);
  arquivo_output2 = fopen(nome_arq_saida_verde, "wb");

  if(!arquivo_output2){
     cout << "Erro ao abrir o arquivo: " << nome_arq_saida_verde << endl;
   }

  ifstream arquivo_saida3(nome_arq_saida_azul);
  arquivo_output3 = fopen(nome_arq_saida_azul, "wb");

  if(!arquivo_output3){
     cout << "Erro ao abrir o arquivo: " << nome_arq_saida_verde << endl;
   }

   fscanf(arquivo_input, "%s\n%s\n%d %d\n%d", numero_magico, mensagem, &largura, &altura, &numero_maximo_cor) ;

   fprintf(arquivo_output1,"P6\n" );
   fprintf(arquivo_output2,"P6\n" );
   fprintf(arquivo_output3,"P6\n" );
   fprintf(arquivo_output1, "%s\n%d %d\n%d\n",mensagem, largura, altura, numero_maximo_cor);
   fprintf(arquivo_output2, "%s\n%d %d %d\n", mensagem, largura, altura, numero_maximo_cor);
   fprintf(arquivo_output3, "%s\n%d %d %d\n", mensagem, largura, altura, numero_maximo_cor);

   for(cont1=5; cont1<=largura-1; cont1++){
     for(cont2=5;cont2<=altura-1;cont2++) {
       fscanf(arquivo_input, "%c%c%c", &R , &G , &B);
       fprintf(arquivo_output1, "%c%c%c" , R , 0 , 0 );
       fprintf(arquivo_output2, "%c%c%c" , 0 , G , 0 );
       fprintf(arquivo_output3, "%c%c%c" , 0 , 0 , B );
     }
    }

    fclose(arquivo_input) ;
    arquivo.close();
    fclose(arquivo_output1);
    arquivo_saida1.close();
    fclose(arquivo_output2);
    arquivo_saida2.close();
    fclose(arquivo_output3);
    arquivo_saida3.close();

}