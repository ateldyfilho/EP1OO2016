#include <iostream>
#include "menu.hpp"
#include <cstdlib>
#include <cstdio>

using namespace std;

Menu::Menu(){
  setOpcao(0);
}

void Menu::setOpcao(int opcao){
  this->opcao = opcao;
}
int Menu::getOpcao(){
  return opcao;
}

void Menu::MenuPrincipal(){
  int numero_menu_principal;

  cout << " " << endl;
  cout << "Tipo de imagem para abrir: " << endl;
  cout << "1 - PPM" << endl;
  cout << "2 - PGM" << endl;
  cout << "Digite uma opção: ";

  cin >> numero_menu_principal;

  if(numero_menu_principal == 1){
    DecifraPPM * decifraPPM = new DecifraPPM();
    decifraPPM -> decifra_ppm();
  }

  else if (numero_menu_principal == 2){
    DecifraPGM * decifraPGM = new DecifraPGM();
    decifraPGM->Segredo_PGM();
  }

}

